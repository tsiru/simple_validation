module Model
  module Validable
    def self.register_validator_method(name)
      eval <<-end_eval
        module ClassMethods
          def validates_#{name}_of(field)
            validates(field, #{name.inspect})
          end
        end
      end_eval
    end

    module ClassMethods
      def validates(field, method)
        validator.add(field, method)
        nil
      end

      def validator
        @validator ||= Model::Validator.new
      end
    end

    module InstanceMethods
      def valid?
        clean_errors!
        self.class.validator.each do |field, validator|
          next if validator.match?(public_send(field))
          errors << validator.error_message_for(field)
        end
        errors.empty?
      end

      def errors
        @errors ||= []
      end

      private

      def validators
        self.class.validators
      end

      def clean_errors!
        @errors = []
      end
    end

    def self.included(base)
      base.include InstanceMethods
      base.extend ClassMethods
    end
  end
end
