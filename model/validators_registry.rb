module Model
  class ValidatorsRegistry
    class Error < StandardError; end

    class << self
      def register_validator(klass, name)
        validators_map[name] = klass

        Validable.register_validator_method(name)
      end

      def find_for!(name)
        validators_map[name] || raise(Error, "missing validator for #{validator.inspect}")
      end

      private

      def validators_map
        @validators_map ||= {}
      end
    end
  end
end
