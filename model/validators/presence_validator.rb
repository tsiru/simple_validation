module Model
  module Validators
    class PresenceValidator < BaseValidator
      register_validator :presence

      def match?(value)
        value && value != ''
      end

      private

      def error_message
        "can't be blank".freeze
      end
    end
  end
end
