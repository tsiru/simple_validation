module Model
  module Validators
    class BaseValidator
      def error_message_for(field)
        "#{field} #{error_message}"
      end

      def match?(value)
        raise NotImplementedError
      end

      private

      def error_message
        raise NotImplementedError
      end

      def self.register_validator(name)
        ValidatorsRegistry.register_validator(self, name)
      end
    end
  end
end
