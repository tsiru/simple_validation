module Model
  module Validators
    class NumericalityValidator < BaseValidator
      register_validator :numericality

      def match?(value)
        value && Float(value)
      rescue ArgumentError => e
        false
      end

      private

      def error_message
        "must be number".freeze
      end
    end
  end
end
