module Model
  class Validator
    def add(field, validator)
      validators_for(field) << build_validator(validator)
      self
    end

    def each
      return enum_for(__method__) unless block_given?

      validators.each do |field, field_validators|
        field_validators.each do |field_validator|
          yield field, field_validator
        end
      end
    end

    private

    def build_validator(validator)
      validator = ValidatorsRegistry.find_for! validator unless validator.is_a? Class
      validator.new
    end

    def validators_for(field)
      validators[field] ||= []
    end

    def validators
      @validators ||= {}
    end
  end
end
