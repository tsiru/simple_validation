require_relative "item"
require "minitest/autorun"

class TestItem < Minitest::Test
  def test_validation_1
    item1 = Item.new(:name => "Guitar")
    assert !item1.valid?
    assert_equal item1.errors, ["price must be number"]
  end

  def test_validation_2
    item2 = Item.new(:price => 55)
    assert !item2.valid?
    assert_equal item2.errors, ["name can't be blank"]
  end

  def test_validation_3
    item3 = Item.new()
    assert !item3.valid?
    assert_equal item3.errors, ["name can't be blank", "price must be number"]
  end

  def test_validation_4
    item4 = Item.new(:name => "Guitar", :price => "ABC")
    assert !item4.valid?
    assert_equal item4.errors, ["price must be number"]
  end

  def test_validation_5
    item5 = Item.new(:name => "Guitar", :price => 55)
    assert item5.valid?
  end
end
